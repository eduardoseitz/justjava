package com.eduardoseitz.justjava;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int quantity = 1;
    int quantityIncrementStep = 1;
    int _coffeePrice = 3;
    int _creamPrice = 1;
    int _chocolatePrice = 2;
    boolean _extraCream = false;
    boolean _extraChocolate = false;
    String _clientName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView quantityText = findViewById(R.id.quantity_text_view);
        final TextView valueText = findViewById(R.id.value_text_view);
        final Button quantityLessButton = findViewById(R.id.quantity_less_button);
        final Button quantityPlusButton = findViewById(R.id.quantity_plus_button);
        final Button orderButton = findViewById(R.id.order_button);
        final CheckBox creamCheckbox = findViewById(R.id.additional_cream_checkbox);
        final CheckBox chocolateCheckbox = findViewById(R.id.additional_coffee_checkbox);
        final EditText clientNameEditText = findViewById(R.id.client_name_text_view);

        // Update labels on app startup
        UpdateTextView(Integer.toString(quantity), quantityText);

        // Quantity decrement button
        quantityLessButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                quantity = IncrementValue(quantity, '-', quantityIncrementStep);
                UpdateTextView(Integer.toString(quantity), quantityText);
            }
        });

        // Quantity increment button
        quantityPlusButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                quantity = IncrementValue(quantity, '+', quantityIncrementStep);
                UpdateTextView(Integer.toString(quantity), quantityText);

            }
        });

        // Order button
        orderButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _clientName = clientNameEditText.getText().toString();
                _extraCream = creamCheckbox.isChecked();
                _extraChocolate = chocolateCheckbox.isChecked();
                int finalValue = CalculatePrice();
                FinishOrder(finalValue, valueText);
            }
        });
    }

    void UpdateTextView(String message, TextView text)
    {
        text.setText(message);
    }

    // Increments or decrements a value by a step value
    int IncrementValue(int value, char incrementType, int stepValue)
    {
        switch(incrementType)
        {
            case '+':
                value += stepValue;
                break;
            case '-':
                if (value - stepValue >= 1)
                {
                    value -= stepValue;
                }
                break;
        }
        return value;
    }

    int CalculatePrice()
    {
        int finalPrice = _coffeePrice;
        if(_extraCream)
            finalPrice += _creamPrice;
        if(_extraChocolate)
            finalPrice += _chocolatePrice;
        finalPrice *= quantity;

        return finalPrice;
    }

    // Update TextView on screen
    void FinishOrder(int value, TextView text)
    {
        String message = "$" + value + ".00";
        UpdateTextView(message, text);

        String eMessage = "-ORDER\n";
        eMessage += quantity + " x coffee.\n";
        eMessage += "\n-EXTRAS\n";
        eMessage += ((_extraCream) ? "Extra " : "No extra ") + "cream.\n";
        eMessage += ((_extraChocolate) ? "Extra " : "No extra ") + "chocolate.\n";
        eMessage += "\nThank You.\n";

        // Send a email intent
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Just Java Coffee Order for " + _clientName);
        intent.putExtra(Intent.EXTRA_TEXT, eMessage);

        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
        else
            Toast.makeText(this, "Email order was not successful", Toast.LENGTH_SHORT).show();
    }
}
